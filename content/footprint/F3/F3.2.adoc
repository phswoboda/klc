+++
title = "F3.2 Resistor naming conventions"
+++

{{< fp_naming_header type="resistor" >}}

== Axial Resistors

{{< fp_code name="r_axial" >}}