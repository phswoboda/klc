+++
title = "F8.1 Non SMD/THT components"
+++

Components which have a footprint on the PCB (and may additionally have a schematic symbol) but do not have an associated physical component which needs to be loaded onto the board during assembly must have their type set to `Other`.
The `Exclude from position file` checkbox must be marked, the `Exclude from BOM` checkbox should be checked.

Examples include:

* Mounting holes
* Solder bridges
* Net ties
* Test points
* Fiducial markings
* Symbols and Logos

{{< klcimg src="F8.1a" title="Set type to 'Other' for non THT/SMD components" >}}

== Exclude from BOM
Some parts should be in the BOM, but not in the position file. Use this for components that need to be mounted manually. 
For those, the `Exclude from BOM` checkbox must stay unchecked.

Examples include:

* Lightpipes
* Screws
* Clip-on heatsinks

